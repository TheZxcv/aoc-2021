use std::env;
use std::fs;
use std::io::{self, BufRead, BufReader};

pub fn input() -> Box<dyn BufRead> {
    let input = env::args().nth(1);
    match input {
        None => Box::new(BufReader::new(io::stdin())),
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
    }
}

pub fn input_lines() -> Vec<String> {
    input().lines().map(|line| line.unwrap()).collect()
}
