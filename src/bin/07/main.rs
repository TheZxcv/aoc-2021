fn main() {
    let first_line = aoc::input_lines().first().unwrap().clone();
    let positions: Vec<i32> = first_line
        .split(',')
        .map(|line| line.parse().unwrap())
        .collect();

    let min_pos = *positions.iter().min().unwrap();
    let max_pos = *positions.iter().max().unwrap();
    let range: Vec<i32> = (min_pos..=max_pos).collect();

    let (_, cost) = find_best_start(&range, &positions, simple_cost);
    println!("01: {}", cost);

    let (_, cost) = find_best_start(&range, &positions, growing_cost);
    println!("02: {}", cost);
}

fn find_best_start(
    starting_points: &Vec<i32>,
    objects: &Vec<i32>,
    cost_fn: fn(i32, &Vec<i32>) -> i32,
) -> (i32, i32) {
    return starting_points
        .iter()
        .map(|&p| (p, cost_fn(p, &objects)))
        .min_by(|a, b| a.1.cmp(&b.1))
        .unwrap();
}

fn simple_cost(start_point: i32, positions: &Vec<i32>) -> i32 {
    positions
        .iter()
        .map(|point| (point - start_point).abs())
        .sum()
}

fn growing_cost(start_point: i32, positions: &Vec<i32>) -> i32 {
    positions
        .iter()
        .map(|point| {
            let distance = (point - start_point).abs();
            return distance * (distance + 1) / 2;
        })
        .sum()
}
