use std::collections::{BTreeSet, HashMap};
use std::iter::FromIterator;

type Digit = BTreeSet<char>;

#[derive(std::fmt::Debug)]
pub struct Signal {
    pub samples: Vec<Digit>,
    pub outputs: Vec<Digit>,
}

impl Signal {
    pub fn new(line: &String) -> Signal {
        let mut splits = line.splitn(2, " | ");
        let samples = splits.next().unwrap().split(" ");
        let outputs = splits.next().unwrap().split(" ");

        Signal {
            samples: samples.map(Signal::build).collect(),
            outputs: outputs.map(Signal::build).collect(),
        }
    }

    fn build(s: &str) -> Digit {
        BTreeSet::from_iter(s.chars())
    }

    pub fn decode(&self) -> i32 {
        let decoding_map = self.build_map();

        let mut value = 0;
        for digit in &self.outputs {
            let decoded = decoding_map.get(&digit).unwrap();
            value = decoded + value * 10;
        }

        value
    }

    fn build_map(&self) -> HashMap<&Digit, i32> {
        let mut decoding_map = HashMap::<&Digit, i32>::new();
        let mut unknowns: Vec<&Digit> = vec![];
        let mut digits: Vec<Option<&Digit>> = vec![None; 10];

        for digit in &self.samples {
            if digit.len() == 2 {
                digits[1] = Some(digit);
            } else if digit.len() == 4 {
                digits[4] = Some(digit);
            } else if digit.len() == 3 {
                digits[7] = Some(digit);
            } else if digit.len() == 7 {
                digits[8] = Some(digit);
            } else {
                unknowns.push(digit);
            }
        }

        /* Patterns:
         * abcde.g: 0
         * ab.....: 1
         * a.cd.fg: 2
         * abcd.f.: 3
         * ab..ef.: 4
         * .bcdef.: 5
         * .bcdefg: 6
         * ab.d...: 7
         * abcdefg: 8
         * abcdef.: 9
         */
        for unknown in unknowns {
            let one = digits[1].unwrap();
            let eight = digits[8].unwrap();
            if unknown.difference(one).count() == 3 {
                assert!(digits[3] == None);
                digits[3] = Some(unknown);
            } else if unknown.intersection(eight).count() == 5 {
                // 2 or 5
                let four = digits[4].unwrap();
                if unknown.intersection(four).count() == 3 {
                    assert!(digits[5] == None);
                    digits[5] = Some(unknown);
                } else {
                    assert!(digits[2] == None);
                    digits[2] = Some(unknown);
                }
            } else if unknown.intersection(eight).count() == 6 {
                // 0 or 6 or 9
                if unknown.intersection(one).count() == 1 {
                    assert!(digits[6] == None);
                    digits[6] = Some(unknown);
                } else {
                    let four = digits[4].unwrap();
                    if unknown.intersection(four).count() == 4 {
                        assert!(digits[9] == None);
                        digits[9] = Some(unknown);
                    } else {
                        assert!(digits[0] == None);
                        digits[0] = Some(unknown);
                    }
                }
            } else {
                unreachable!();
            }
        }

        for (i, digit) in digits.iter().enumerate() {
            decoding_map.insert(digit.unwrap(), i as i32);
        }

        decoding_map
    }
}

fn main() {
    let lines = aoc::input_lines();
    let signals = lines.iter().map(Signal::new).collect::<Vec<_>>();

    let simple_count = signals
        .iter()
        .flat_map(|signal| &signal.outputs)
        .filter(|digit| {
            digit.len() == 2 /* 1 */
        || digit.len() == 4 /* 4 */
        || digit.len() == 3 /* 7 */
        || digit.len() == 7 /* 8 */
        })
        .count();

    println!("01: {}", simple_count);

    let total: i32 = signals.iter().map(|signal| signal.decode()).sum();
    println!("02: {}", total);
}
