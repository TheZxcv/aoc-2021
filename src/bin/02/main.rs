fn main() {
    let lines = aoc::input_lines();

    let mut horizontal = 0;
    let mut depth = 0;
    for line in &lines {
        if line.starts_with("forward") {
            let value = parse(line, "forward".len());
            horizontal += value;
        } else if line.starts_with("up") {
            let value = parse(line, "up".len());
            depth -= value;
        } else if line.starts_with("down") {
            let value = parse(line, "down".len());
            depth += value;
        } else {
            unreachable!();
        }
    }
    println!("01: {}", horizontal * depth);

    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;
    for line in &lines {
        if line.starts_with("forward") {
            let value = parse(line, "forward".len());
            horizontal += value;
            depth += aim * value;
        } else if line.starts_with("up") {
            let value = parse(line, "up".len());
            aim -= value;
        } else if line.starts_with("down") {
            let value = parse(line, "down".len());
            aim += value;
        } else {
            unreachable!();
        }
    }
    println!("02: {}", horizontal * depth);
}

fn parse(line: &str, prefix_len: usize) -> i32 {
    let start = prefix_len + 1;
    let end = line.len();
    line[start..end].parse().unwrap()
}
