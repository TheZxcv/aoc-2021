pub struct TargetArea {
    pub x: (i32, i32),
    pub y: (i32, i32),
}

impl TargetArea {
    pub fn new(input: &String) -> TargetArea {
        if !input.starts_with("target area: x=") {
            panic!("Invalid input format.");
        }

        let comma_idx = input.find(',').unwrap();
        let x_slice = &input[input.find("x=").unwrap() + 2..comma_idx];
        let y_slice = &input[input.find("y=").unwrap() + 2..];

        TargetArea {
            x: TargetArea::parse_range(x_slice),
            y: TargetArea::parse_range(y_slice),
        }
    }

    /// Parses `-5..5` expression.
    fn parse_range(range: &str) -> (i32, i32) {
        let dot_idx = range.find("..").unwrap();
        let start = range[..dot_idx].parse().unwrap();
        let end = range[dot_idx + 2..].parse().unwrap();

        (start, end)
    }

    pub fn contains(&self, (x, y): (i32, i32)) -> bool {
        (self.x.0 <= x && x <= self.x.1) && (self.y.0 <= y && y <= self.y.1)
    }
}

fn simulate(velocity: (i32, i32), area: &TargetArea) -> Option<i32> {
    let mut velocity = velocity;
    let mut position = (0, 0);
    let mut max_height: i32 = 0;

    while position.0 < area.x.1 && position.1 > area.y.0 {
        position = (position.0 + velocity.0, position.1 + velocity.1);

        if velocity.0 > 0 {
            velocity.0 -= 1;
        } else if velocity.0 < 0 {
            velocity.0 += 1;
        }
        velocity.1 -= 1;

        max_height = i32::max(max_height, position.1);

        if area.contains(position) {
            return Some(max_height);
        }
    }

    None
}

// Horrible way to solve it...
fn main() {
    let line = aoc::input_lines().first().unwrap().to_owned();
    let target_area = TargetArea::new(&line);

    let max_height = (0..10000)
        .flat_map(|x| (-10000..10000).into_iter().map(move |y| (x, y)))
        .filter_map(|velocity| simulate(velocity, &target_area))
        .max()
        .unwrap();
    println!("01: {}", max_height);

    let count = (0..10000)
        .flat_map(|x| (-10000..10000).into_iter().map(move |y| (x, y)))
        .filter_map(|velocity| simulate(velocity, &target_area))
        .count();
    println!("02: {}", count);
}
