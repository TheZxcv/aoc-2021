use std::collections::HashSet;

type Cube = (i32, i32, i32);

enum Command {
    On,
    Off,
}

#[derive(Copy, Clone)]
struct Range {
    start: i32,
    end: i32, // non-inclusive
}

impl Range {
    pub fn empty() -> Range {
        Range { start: 0, end: 0 }
    }

    pub fn is_empty(&self) -> bool {
        self.start >= self.end
    }

    pub fn intersect(&self, other: &Range) -> Range {
        if self.start >= other.end || self.end <= other.start {
            Range::empty()
        } else {
            Range {
                start: i32::max(self.start, other.start),
                end: i32::min(self.end, other.end),
            }
        }
    }

    pub fn subtract(&self, other: &Range) -> (Range, Range) {
        if self.is_empty() {
            return (Range::empty(), Range::empty());
        }

        if other.is_empty() {
            return (*self, Range::empty());
        }

        let left_side = if self.start < other.start {
            Range {
                start: self.start,
                end: i32::min(self.end, other.start),
            }
        } else {
            Range::empty()
        };

        let right_side = if self.end > other.end {
            Range {
                start: i32::max(self.start, other.end),
                end: self.end,
            }
        } else {
            Range::empty()
        };

        (left_side, right_side)
    }

    pub fn length(&self) -> i32 {
        self.end - self.start
    }
}

struct Cuboid {
    x: Range,
    y: Range,
    z: Range,
}

impl Cuboid {
    pub fn new(x: Range, y: Range, z: Range) -> Cuboid {
        Cuboid { x, y, z }
    }

    pub fn subtract(&self, other: &Cuboid) -> Vec<Cuboid> {
        let mut sections = Vec::new();

        let (left_x, right_x) = self.x.subtract(&other.x);
        let (left_y, right_y) = self.y.subtract(&other.y);
        let (left_z, right_z) = self.z.subtract(&other.z);
        // Front view
        // AA AA AA
        // BB XX DD
        // DD DD DD

        // Top view
        // BB CC DD
        // BB XX DD
        // BB EE DD

        sections.push(Cuboid::new(self.x, left_y, self.z)); // AAAAAA
        sections.push(Cuboid::new(self.x, right_y, self.z)); // DDDDDD

        sections.push(Cuboid::new(left_x, other.y.intersect(&self.y), self.z)); // BB
        sections.push(Cuboid::new(right_x, other.y.intersect(&self.y), self.z)); // DD

        sections.push(Cuboid::new(
            other.x.intersect(&self.x),
            other.y.intersect(&self.y),
            left_z,
        )); // EE
        sections.push(Cuboid::new(
            other.x.intersect(&self.x),
            other.y.intersect(&self.y),
            right_z,
        )); // CC

        sections
            .into_iter()
            .filter(|c| !c.x.is_empty() && !c.y.is_empty() && !c.z.is_empty())
            .collect::<Vec<Cuboid>>()
    }

    pub fn volume(&self) -> i64 {
        self.x.length() as i64 * self.y.length() as i64 * self.z.length() as i64
    }
}

struct RebootStep {
    command: Command,
    x: Range,
    y: Range,
    z: Range,
}

fn main() {
    let steps = aoc::input_lines()
        .iter()
        .filter_map(|line| read_step(line))
        .collect::<Vec<_>>();

    let mut cubes_lit = HashSet::<Cube>::new();
    for step in &steps {
        let x_range = clamp(&step.x);
        let y_range = clamp(&step.y);
        let z_range = clamp(&step.z);
        for x in x_range.start..x_range.end {
            for y in y_range.start..y_range.end {
                for z in z_range.start..z_range.end {
                    if let Command::On = step.command {
                        cubes_lit.insert((x, y, z));
                    } else {
                        cubes_lit.remove(&(x, y, z));
                    }
                }
            }
        }
    }
    println!("01: {}", cubes_lit.len());

    let mut cuboids: Vec<Cuboid> = Vec::new();
    for step in steps {
        let cuboid = Cuboid::new(step.x, step.y, step.z);

        // Remove current cuboid from all previous.
        cuboids = cuboids
            .iter()
            .flat_map(|c| c.subtract(&cuboid))
            .collect::<Vec<_>>();
        if let Command::On = step.command {
            cuboids.push(cuboid);
        }
    }

    let cubes_lit = cuboids.iter().map(|r| r.volume()).sum::<i64>();
    println!("02: {}", cubes_lit);
}

fn clamp(range: &Range) -> Range {
    Range {
        start: i32::max(range.start, -50),
        end: i32::min(range.end, 51),
    }
}

fn read_step(line: &str) -> Option<RebootStep> {
    let command = if line.starts_with("on ") {
        Some(Command::On)
    } else if line.starts_with("off ") {
        Some(Command::Off)
    } else {
        None
    };
    if command.is_none() {
        return None;
    }

    let space_index = line.find(' ').unwrap();
    let ranges = line[space_index + 1..]
        .split(',')
        .map(|chunk| parse_range(chunk))
        .collect::<Vec<_>>();
    assert!(ranges.len() == 3, "Unexpected amount of ranges.");

    Some(RebootStep {
        command: command.unwrap(),
        x: ranges[0],
        y: ranges[1],
        z: ranges[2],
    })
}

fn parse_range(input: &str) -> Range {
    let dot = input.find('.').unwrap();

    Range {
        start: input[2..dot].parse::<i32>().unwrap(),
        end: input[dot + 2..].parse::<i32>().unwrap() + 1,
    }
}
