pub struct BitBuffer {
    source: Vec<char>,
    outer_index: usize,
    inner_index: usize,
}

impl BitBuffer {
    pub fn new(chars: Vec<char>) -> BitBuffer {
        BitBuffer {
            source: chars,
            outer_index: 0,
            inner_index: 0,
        }
    }

    pub fn has_more(&self) -> bool {
        self.outer_index < self.source.len()
    }

    pub fn bits_read(&self) -> usize {
        4 * self.outer_index + self.inner_index
    }

    fn consume(&mut self) -> Option<u8> {
        if !self.has_more() {
            return None;
        }

        assert!(self.inner_index < 4);

        let mask: u8 = 1 << (4 - self.inner_index - 1);
        let nibble = self.source[self.outer_index].to_digit(16).unwrap() as u8;
        let bit = if nibble & mask != 0 { 1 } else { 0 };

        self.inner_index += 1;
        if self.inner_index == 4 {
            self.inner_index = 0;
            self.outer_index += 1;
        }

        Some(bit)
    }

    pub fn read_n_bits(&mut self, bits: usize) -> u64 {
        assert!(bits <= 64);

        let mut value = 0u64;
        for _ in 0..bits {
            value <<= 1;
            value |= self.consume().unwrap() as u64;
        }

        value
    }
}

pub enum PacketContent {
    Literal(u64),
    Operator(Box<Vec<Packet>>),
}

pub struct Packet {
    version: u8,
    type_id: u8,
    content: PacketContent,
}

impl Packet {
    pub fn read(buffer: &mut BitBuffer) -> Packet {
        let version = buffer.read_n_bits(3) as u8;
        let type_id = buffer.read_n_bits(3) as u8;

        match type_id {
            4 => Packet::literal(buffer, version, type_id),
            _ => Packet::operator(buffer, version, type_id),
        }
    }

    fn literal(buffer: &mut BitBuffer, version: u8, type_id: u8) -> Packet {
        let mut bits = 0;
        let mut value = 0;
        loop {
            let literal = buffer.read_n_bits(5);
            let has_more = literal & 0x10 != 0;
            value = (value << 4) | (literal & 0x0f);

            bits += 4;
            assert!(bits <= 64, "Integer overflow while reading literal.");

            if !has_more {
                break;
            }
        }

        Packet {
            version,
            type_id,
            content: PacketContent::Literal(value),
        }
    }

    fn operator(buffer: &mut BitBuffer, version: u8, type_id: u8) -> Packet {
        let mut sub_packets = Vec::new();

        let length_type_id = buffer.read_n_bits(1);
        if length_type_id == 0 {
            let length = buffer.read_n_bits(15) as usize;

            let final_len = buffer.bits_read() + length;
            while buffer.bits_read() < final_len {
                let packet = Packet::read(buffer);
                sub_packets.push(packet);
            }

            assert!(final_len == final_len);
        } else if length_type_id == 1 {
            let length = buffer.read_n_bits(11);

            for _ in 0..length {
                let packet = Packet::read(buffer);
                sub_packets.push(packet);
            }
        } else {
            unreachable!();
        }

        Packet {
            version,
            type_id,
            content: PacketContent::Operator(Box::new(sub_packets)),
        }
    }
}

impl Packet {
    pub fn eval(&self) -> u64 {
        match &self.content {
            PacketContent::Literal(value) => *value as u64,
            PacketContent::Operator(body) => Packet::apply(self.type_id, body),
        }
    }

    fn apply(type_id: u8, body: &Vec<Packet>) -> u64 {
        match type_id {
            0 => Packet::sum(body),
            1 => Packet::product(body),
            2 => Packet::minimum(body),
            3 => Packet::maximum(body),
            5 => Packet::greater_than(body),
            6 => Packet::less_than(body),
            7 => Packet::equal_to(body),
            _ => unreachable!(),
        }
    }

    fn sum(body: &Vec<Packet>) -> u64 {
        assert!(body.len() > 0);

        body.iter().map(|p| p.eval()).sum()
    }

    fn product(body: &Vec<Packet>) -> u64 {
        assert!(body.len() > 0);

        body.iter().map(|p| p.eval()).fold(1, |acc, e| acc * e)
    }

    fn minimum(body: &Vec<Packet>) -> u64 {
        body.iter().map(|p| p.eval()).min().unwrap()
    }

    fn maximum(body: &Vec<Packet>) -> u64 {
        body.iter().map(|p| p.eval()).max().unwrap()
    }

    fn greater_than(body: &Vec<Packet>) -> u64 {
        assert!(body.len() == 2);

        let first = body.first().unwrap();
        let second = body.last().unwrap();

        if first.eval() > second.eval() {
            1
        } else {
            0
        }
    }

    fn less_than(body: &Vec<Packet>) -> u64 {
        assert!(body.len() == 2);

        let first = body.first().unwrap();
        let second = body.last().unwrap();

        if first.eval() < second.eval() {
            1
        } else {
            0
        }
    }

    fn equal_to(body: &Vec<Packet>) -> u64 {
        assert!(body.len() == 2);

        let first = body.first().unwrap();
        let second = body.last().unwrap();

        if first.eval() == second.eval() {
            1
        } else {
            0
        }
    }
}

fn sum_versions(packet: &Packet) -> usize {
    packet.version as usize
        + match &packet.content {
            PacketContent::Literal(_) => 0,
            PacketContent::Operator(sub_packets) => sub_packets.iter().map(sum_versions).sum(),
        }
}

fn main() {
    let line: String = aoc::input_lines().first().unwrap().to_owned();
    let chars = line.chars().collect::<Vec<_>>();
    let mut buffer = BitBuffer::new(chars);

    let packet = Packet::read(&mut buffer);

    let sum = sum_versions(&packet);
    println!("01: {}", sum);

    let result = packet.eval();
    println!("02: {}", result);
}
