#[derive(Copy, Clone, Eq, PartialEq, Debug)]
enum AmphipodType {
    Amber = 0,
    Bronze = 1,
    Copper = 2,
    Desert = 3,
}

struct Room {
    pub kind: AmphipodType,
    max_size: usize,
    content: Vec<AmphipodType>,
    cant_leave: bool,
}

impl Room {
    pub fn new(kind: AmphipodType, size: usize, initial_content: Vec<AmphipodType>) -> Room {
        let cant_leave = initial_content.iter().all(|&t| t == kind);
        Room {
            kind: kind,
            max_size: size,
            content: initial_content,
            cant_leave: cant_leave,
        }
    }

    pub fn can_enter(&self, kind: AmphipodType) -> bool {
        self.kind == kind && self.cant_leave
    }

    pub fn is_complete(&self) -> bool {
        self.content.len() == self.max_size && self.cant_leave
    }

    pub fn entrance_only(&self) -> bool {
        self.cant_leave
    }

    pub fn peek(&self) -> Option<AmphipodType> {
        if let Some(kind) = self.content.last() {
            Some(*kind)
        } else {
            None
        }
    }

    pub fn pop(&mut self) -> (i64, AmphipodType) {
        let kind = self.content.pop().unwrap();
        self.cant_leave = self.content.iter().all(|&t| t == self.kind);

        ((self.max_size - self.content.len()) as i64, kind)
    }

    pub fn insert(&mut self, kind: AmphipodType) -> i64 {
        let steps = (self.max_size - self.content.len()) as i64;
        self.content.push(kind);
        self.cant_leave = self.content.iter().all(|&t| t == self.kind);

        steps
    }
}

struct Board {
    hallway: [Option<AmphipodType>; 11],
    rooms: [Room; 4],
}

impl std::fmt::Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "#############")?;
        write!(f, "#")?;
        for spot in self.hallway {
            write!(f, "{}", Board::type_to_char(spot))?;
        }
        writeln!(f, "#")?;

        let amber = &self.rooms[0];
        let bronze = &self.rooms[1];
        let copper = &self.rooms[2];
        let desert = &self.rooms[3];

        write!(f, "###")?;
        write!(f, "{}", Board::type_to_char(amber.content.get(1).copied()))?;
        write!(f, "#")?;
        write!(f, "{}", Board::type_to_char(bronze.content.get(1).copied()))?;
        write!(f, "#")?;
        write!(f, "{}", Board::type_to_char(copper.content.get(1).copied()))?;
        write!(f, "#")?;
        write!(f, "{}", Board::type_to_char(desert.content.get(1).copied()))?;
        writeln!(f, "###")?;
        for i in (0..amber.max_size - 1).rev() {
            write!(f, "  #")?;
            write!(f, "{}", Board::type_to_char(amber.content.get(i).copied()))?;
            write!(f, "#")?;
            write!(f, "{}", Board::type_to_char(bronze.content.get(i).copied()))?;
            write!(f, "#")?;
            write!(f, "{}", Board::type_to_char(copper.content.get(i).copied()))?;
            write!(f, "#")?;
            write!(f, "{}", Board::type_to_char(desert.content.get(i).copied()))?;
            writeln!(f, "#")?;
        }

        writeln!(f, "  #########")
    }
}

impl Board {
    pub fn parse(lines: &Vec<String>) -> Board {
        assert!(lines.len() == 5 || lines.len() == 7);
        assert!(lines[0] == "#############");
        assert!(lines[4] == "  #########" || lines[6] == "  #########");

        let room_size = lines.len() - 3;

        let mut hallway = [None; 11];
        for (i, ch) in lines[1]
            .chars()
            .into_iter()
            .filter(|&ch| ch != '#')
            .enumerate()
        {
            hallway[i] = Board::char_to_type(ch);
        }

        Board {
            hallway,
            rooms: [
                Room::new(
                    AmphipodType::Amber,
                    room_size,
                    lines[2..]
                        .iter()
                        .map(|l| l.chars().collect::<Vec<_>>())
                        .filter_map(|chars| Board::char_to_type(chars[3]))
                        .rev()
                        .collect::<Vec<_>>(),
                ),
                Room::new(
                    AmphipodType::Bronze,
                    room_size,
                    lines[2..]
                        .iter()
                        .map(|l| l.chars().collect::<Vec<_>>())
                        .filter_map(|chars| Board::char_to_type(chars[5]))
                        .rev()
                        .collect::<Vec<_>>(),
                ),
                Room::new(
                    AmphipodType::Copper,
                    room_size,
                    lines[2..]
                        .iter()
                        .map(|l| l.chars().collect::<Vec<_>>())
                        .filter_map(|chars| Board::char_to_type(chars[7]))
                        .rev()
                        .collect::<Vec<_>>(),
                ),
                Room::new(
                    AmphipodType::Desert,
                    room_size,
                    lines[2..]
                        .iter()
                        .map(|l| l.chars().collect::<Vec<_>>())
                        .filter_map(|chars| Board::char_to_type(chars[9]))
                        .rev()
                        .collect::<Vec<_>>(),
                ),
            ],
        }
    }

    pub fn move_to_room(&mut self, starting_position: usize, room_index: usize) -> i64 {
        let kind = self.hallway[starting_position].unwrap();
        self.hallway[starting_position] = None;
        let steps_to_enter = self.rooms[room_index].insert(kind);

        Board::cost(kind)
            * (Board::distance(starting_position, Board::room_to_position(room_index))
                + steps_to_enter)
    }

    pub fn undo_move_to_room(&mut self, starting_position: usize, room_index: usize) {
        let (_, kind) = self.rooms[room_index].pop();
        self.hallway[starting_position] = Some(kind);
    }

    pub fn move_room_to_room(&mut self, start_room: usize, end_room: usize) -> i64 {
        let (steps_to_leave, kind) = self.rooms[start_room].pop();
        let steps_to_enter = self.rooms[end_room].insert(kind);

        Board::cost(kind)
            * (Board::distance(
                Board::room_to_position(start_room),
                Board::room_to_position(end_room),
            ) + steps_to_leave
                + steps_to_enter)
    }

    pub fn undo_move_room_to_room(&mut self, start_room: usize, end_room: usize) {
        let (_, kind) = self.rooms[end_room].pop();
        let _ = self.rooms[start_room].insert(kind);
    }

    pub fn move_to_hallway(&mut self, start_room: usize, destination: usize) -> i64 {
        let (steps_to_leave, kind) = self.rooms[start_room].pop();
        self.hallway[destination] = Some(kind);

        Board::cost(kind)
            * (Board::distance(Board::room_to_position(start_room), destination) + steps_to_leave)
    }

    pub fn undo_move_to_hallway(&mut self, start_room: usize, destination: usize) {
        let kind = self.hallway[destination].unwrap();
        self.hallway[destination] = None;
        let _ = self.rooms[start_room].insert(kind);
    }

    pub fn is_solved(&self) -> bool {
        self.rooms.iter().all(|r| r.is_complete())
    }

    fn distance(start: usize, end: usize) -> i64 {
        if end > start {
            (end - start) as i64
        } else {
            (start - end) as i64
        }
    }

    fn cost(kind: AmphipodType) -> i64 {
        match kind {
            AmphipodType::Amber => 1,
            AmphipodType::Bronze => 10,
            AmphipodType::Copper => 100,
            AmphipodType::Desert => 1000,
        }
    }

    pub fn room_to_position(room_index: usize) -> usize {
        match room_index {
            0 => 2,
            1 => 4,
            2 => 6,
            3 => 8,
            _ => panic!(),
        }
    }

    fn char_to_type(ch: char) -> Option<AmphipodType> {
        match ch {
            'A' => Some(AmphipodType::Amber),
            'B' => Some(AmphipodType::Bronze),
            'C' => Some(AmphipodType::Copper),
            'D' => Some(AmphipodType::Desert),
            _ => None,
        }
    }

    fn type_to_char(value: Option<AmphipodType>) -> char {
        match value {
            Some(AmphipodType::Amber) => 'A',
            Some(AmphipodType::Bronze) => 'B',
            Some(AmphipodType::Copper) => 'C',
            Some(AmphipodType::Desert) => 'D',
            None => '.',
        }
    }

    pub fn is_path_free(&self, start: usize, end: usize) -> bool {
        let start = if start < end { start + 1 } else { start - 1 };

        let a = usize::min(start, end);
        let b = usize::max(start, end);
        self.hallway[a..=b].iter().all(|c| c.is_none())
    }
}

fn main() {
    let lines = aoc::input_lines();
    let mut board = Board::parse(&lines);

    let energy = simulate(&mut board);
    println!("01: {}", energy);

    let mut expanded_board = {
        let mut lines = lines.clone();
        lines.insert(3, "  #D#C#B#A#".to_string());
        lines.insert(4, "  #D#B#A#C#".to_string());
        Board::parse(&lines)
    };

    let energy = simulate(&mut expanded_board);
    println!("02: {}", energy);
}

fn simulate(mut board: &mut Board) -> i64 {
    if board.is_solved() {
        return 0;
    }

    let mut energy: i64 = i64::MAX;

    for i in 0..board.hallway.len() {
        if let Some(kind) = board.hallway[i] {
            let j = kind as usize;
            if board.rooms[j].can_enter(kind) && board.is_path_free(i, Board::room_to_position(j)) {
                let used_energy = board.move_to_room(i, j);
                if used_energy > energy {
                    board.undo_move_to_room(i, j);
                    continue;
                }
                let rest = simulate(&mut board);
                board.undo_move_to_room(i, j);

                if rest == i64::MAX {
                    continue;
                }

                energy = i64::min(energy, used_energy + rest);
            }
        }
    }

    for i in 0..board.rooms.len() {
        if board.rooms[i].entrance_only() {
            continue;
        }

        if let Some(kind) = board.rooms[i].peek() {
            let j = kind as usize;
            if i != j
                && board.rooms[i].can_enter(kind)
                && board.is_path_free(Board::room_to_position(i), Board::room_to_position(j))
            {
                let used_energy = board.move_room_to_room(i, j);
                if used_energy > energy {
                    board.undo_move_room_to_room(i, j);
                    continue;
                }

                let rest = simulate(&mut board);
                board.undo_move_room_to_room(i, j);

                if rest == i64::MAX {
                    continue;
                }

                energy = i64::min(energy, used_energy + rest);
            }
        }
    }

    for i in 0..board.rooms.len() {
        if board.rooms[i].entrance_only() {
            continue;
        }

        if board.rooms[i].peek().is_some() {
            let exit_index = Board::room_to_position(i);

            for destination in (exit_index + 1)..11 {
                if board.hallway[destination].is_some() {
                    break;
                }

                if destination != 2 && destination != 4 && destination != 6 && destination != 8 {
                    let used_energy = board.move_to_hallway(i, destination);
                    if used_energy > energy {
                        board.undo_move_to_hallway(i, destination);
                        continue;
                    }

                    let rest = simulate(&mut board);
                    board.undo_move_to_hallway(i, destination);

                    if rest == i64::MAX {
                        continue;
                    }

                    energy = i64::min(energy, used_energy + rest);
                }
            }

            for destination in (0..exit_index).rev() {
                if board.hallway[destination].is_some() {
                    break;
                }

                if destination != 2 && destination != 4 && destination != 6 && destination != 8 {
                    let used_energy = board.move_to_hallway(i, destination);
                    if used_energy > energy {
                        board.undo_move_to_hallway(i, destination);
                        continue;
                    }

                    let rest = simulate(&mut board);
                    board.undo_move_to_hallway(i, destination);

                    if rest == i64::MAX {
                        continue;
                    }

                    energy = i64::min(energy, used_energy + rest);
                }
            }
        }
    }

    energy
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let lines = vec![
            "#############".to_string(),
            "#...........#".to_string(),
            "###B#C#B#D###".to_string(),
            "  #A#D#C#A#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(12521, energy);
    }

    #[test]
    fn test_example_part2() {
        let lines = vec![
            "#############".to_string(),
            "#...........#".to_string(),
            "###B#C#B#D###".to_string(),
            "  #D#C#B#A#".to_string(),
            "  #D#B#A#C#".to_string(),
            "  #A#D#C#A#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(44169, energy);
    }

    #[test]
    fn test_one() {
        let lines = vec![
            "#############".to_string(),
            "#A..........#".to_string(),
            "###.#B#C#D###".to_string(),
            "  #A#B#C#D#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(3, energy);
    }

    #[test]
    fn test_two() {
        let lines = vec![
            "#############".to_string(),
            "#.....D.D.A.#".to_string(),
            "###.#B#C#.###".to_string(),
            "  #A#B#C#.#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(7008, energy);
    }

    #[test]
    fn test_three() {
        let lines = vec![
            "#############".to_string(),
            "#.....D.....#".to_string(),
            "###.#B#C#D###".to_string(),
            "  #A#B#C#A#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(9011, energy);
    }

    #[test]
    fn test_input() {
        let lines = vec![
            "#############".to_string(),
            "#...........#".to_string(),
            "###D#A#C#C###".to_string(),
            "  #D#A#B#B#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(19046, energy);
    }

    #[test]
    fn test_input_part2() {
        let lines = vec![
            "#############".to_string(),
            "#...........#".to_string(),
            "###D#A#C#C###".to_string(),
            "  #D#C#B#A#".to_string(),
            "  #D#B#A#C#".to_string(),
            "  #D#A#B#B#".to_string(),
            "  #########".to_string(),
        ];

        let energy = simulate(&mut Board::parse(&lines));
        assert_eq!(47484, energy);
    }
}
