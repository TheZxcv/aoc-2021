use std::collections::HashMap;

fn main() {
    let lines = aoc::input_lines();
    let seed_pairs = into_pairs(&lines[0]);

    let rules: HashMap<(char, char), char> = lines
        .iter()
        .skip(2)
        .map(|line| {
            let mut splits = line.splitn(2, " -> ");

            let head = splits.next().unwrap();
            assert!(head.len() == 2);
            let head = (head.chars().nth(0).unwrap(), head.chars().nth(1).unwrap());

            let tail = splits.next().unwrap();
            assert!(tail.len() == 1);
            let tail = tail.chars().nth(0).unwrap();

            (head, tail)
        })
        .fold(HashMap::new(), |mut map, (head, tail)| {
            map.insert(head, tail);
            map
        });

    let mut cache = HashMap::<(i32, (char, char)), HashMap<char, u64>>::new();
    let mut counters = HashMap::<char, u64>::new();
    for &pair in &seed_pairs {
        let update = calculate_counters(&rules, pair, 10, &mut cache);
        merge(&mut counters, &update);
    }
    for i in 0..seed_pairs.len() - 1 {
        let (_, tail) = seed_pairs[i];
        let entry = counters.get_mut(&tail).unwrap();
        *entry -= 1;
    }
    let subtraction = {
        let min = counters.values().min().unwrap();
        let max = counters.values().max().unwrap();
        max - min
    };
    println!("01: {}", subtraction);

    let mut counters = HashMap::<char, u64>::new();
    for &pair in &seed_pairs {
        let update = calculate_counters(&rules, pair, 40, &mut cache);
        merge(&mut counters, &update);
    }
    for i in 0..seed_pairs.len() - 1 {
        let (_, tail) = seed_pairs[i];
        let entry = counters.get_mut(&tail).unwrap();
        *entry -= 1;
    }
    let subtraction = {
        let min = counters.values().min().unwrap();
        let max = counters.values().max().unwrap();
        max - min
    };
    println!("02: {}", subtraction);
}

fn calculate_counters(
    rules: &HashMap<(char, char), char>,
    pair: (char, char),
    depth: i32,
    cache: &mut HashMap<(i32, (char, char)), HashMap<char, u64>>,
) -> HashMap<char, u64> {
    let mut counters = HashMap::<char, u64>::new();
    if depth == 0 {
        let (head, tail) = pair;
        let entry = counters.entry(head).or_insert(0);
        *entry += 1;
        let entry = counters.entry(tail).or_insert(0);
        *entry += 1;
        return counters;
    }

    if let Some(cached) = cache.get(&(depth, pair)) {
        merge(&mut counters, &cached);
        return counters;
    }

    if let Some(rule) = rules.get(&pair) {
        let (head, tail) = pair;

        let left = calculate_counters(rules, (head, *rule), depth - 1, cache);
        merge(&mut counters, &left);

        let right = calculate_counters(rules, (*rule, tail), depth - 1, cache);
        merge(&mut counters, &right);

        let entry = counters.get_mut(rule).unwrap();
        *entry -= 1;
    } else {
        let (head, tail) = pair;
        let entry = counters.entry(head).or_insert(0);
        *entry += 1;
        let entry = counters.entry(tail).or_insert(0);
        *entry += 1;
    }

    cache.insert((depth, pair), counters.clone());

    counters
}

fn into_pairs(s: &str) -> Vec<(char, char)> {
    let mut pairs = Vec::new();
    for index in 0..s.len() - 1 {
        let first = s.chars().nth(index).unwrap();
        let second = s.chars().nth(index + 1).unwrap();
        pairs.push((first, second));
    }

    pairs
}

fn merge(a: &mut HashMap<char, u64>, b: &HashMap<char, u64>) {
    for key in b.keys() {
        let entry = a.entry(*key).or_insert(0);
        *entry += b.get(key).unwrap();
    }
}
