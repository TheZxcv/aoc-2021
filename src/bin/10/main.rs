fn main() {
    let lines = aoc::input_lines();

    let (corrects, illegal_chars): (Vec<_>, Vec<_>) = lines
        .iter()
        .map(|line| syntax_check(&line))
        .partition(Result::is_ok);

    let corrects = corrects.into_iter().map(Result::unwrap).collect::<Vec<_>>();
    let illegal_chars = illegal_chars
        .into_iter()
        .map(Result::unwrap_err)
        .collect::<Vec<_>>();

    let errors_score: u32 = illegal_chars.iter().map(|&ch| illegal_char_score(ch)).sum();
    println!("01: {}", errors_score);

    let mut scores: Vec<u64> = corrects
        .iter()
        .map(|incomplete| score_incomplete(&incomplete))
        .collect();
    scores.sort();

    let score = scores[scores.len() / 2];
    println!("02: {}", score);
}

fn syntax_check(line: &str) -> Result<Vec<char>, char> {
    let mut stack: Vec<char> = Vec::new();

    for ch in line.chars() {
        match ch {
            '(' | '[' | '{' | '<' => stack.push(ch),

            ')' => {
                if Some('(') != stack.last().copied() {
                    return Err(ch);
                }
                stack.pop();
            }
            ']' => {
                if Some('[') != stack.last().copied() {
                    return Err(ch);
                }
                stack.pop();
            }
            '}' => {
                if Some('{') != stack.last().copied() {
                    return Err(ch);
                }
                stack.pop();
            }
            '>' => {
                if Some('<') != stack.last().copied() {
                    return Err(ch);
                }
                stack.pop();
            }

            _ => panic!("Unexpected character."),
        }
    }

    Ok(stack)
}

fn illegal_char_score(ch: char) -> u32 {
    match ch {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => panic!("Unexpected character."),
    }
}

fn unmatched_char_score(ch: char) -> u32 {
    match ch {
        '(' => 1,
        '[' => 2,
        '{' => 3,
        '<' => 4,
        _ => panic!("Unexpected character."),
    }
}

fn score_incomplete(stack_unmatched: &Vec<char>) -> u64 {
    stack_unmatched
        .into_iter()
        .rev()
        .map(|&ch| unmatched_char_score(ch) as u64)
        .fold(0, |acc, score| acc * 5 + score)
}
