use std::collections::HashMap;

pub struct Node {
    pub index: usize,
    pub name: String,
    pub allow_reentrance: bool,
}

pub struct Graph {
    name_to_node: HashMap<String, usize>,
    pub nodes: Vec<Node>,
    pub edges: Vec<Vec<usize>>,
}

impl Graph {
    pub fn new() -> Graph {
        Graph {
            name_to_node: HashMap::new(),
            edges: Vec::new(),
            nodes: Vec::new(),
        }
    }

    pub fn add_edge(&mut self, start: &str, end: &str) {
        let start_idx = self.get_or_create_index(start);
        let end_idx = self.get_or_create_index(end);

        // Do not add edges that enter the start node or leave the end node.
        if start != "end" && end != "start" {
            self.edges[start_idx].push(end_idx);
        }
        if start != "start" && end != "end" {
            self.edges[end_idx].push(start_idx);
        }
    }

    fn get_or_create_index(&mut self, name: &str) -> usize {
        let index = self.name_to_node.entry(name.to_string()).or_insert({
            let index = self.nodes.len();
            self.edges.push(Vec::new());
            self.nodes.push(Node {
                index,
                name: name.to_string(),
                allow_reentrance: name.chars().nth(0).unwrap().is_uppercase(),
            });

            index
        });

        *index
    }

    fn get_node(&self, name: &str) -> &Node {
        let index = self.name_to_node.get(name).unwrap();
        &self.nodes[*index]
    }

    pub fn count_paths(&self, allow_reentrance_once: bool) -> u32 {
        let mut markers = vec![false; self.nodes.len()];

        let source = self.get_node("start");
        let target = self.get_node("end");
        self.count_paths_internal(source, target, allow_reentrance_once, &mut markers)
    }

    fn count_paths_internal(
        &self,
        source: &Node,
        target: &Node,
        allow_reentrance_once: bool,
        markers: &mut Vec<bool>,
    ) -> u32 {
        let already_marked = markers[source.index];
        markers[source.index] = true;

        let mut paths = 0;
        for &i in &self.edges[source.index] {
            let node = &self.nodes[i];
            if i == target.index {
                paths += 1;
            } else if node.allow_reentrance || !markers[i] || allow_reentrance_once {
                let allow_reentrance_once =
                    (node.allow_reentrance || !markers[i]) && allow_reentrance_once;
                paths += self.count_paths_internal(node, target, allow_reentrance_once, markers);
            }
        }

        if !already_marked {
            markers[source.index] = false;
        }

        paths
    }
}

fn main() {
    let lines = aoc::input_lines();
    let graph = lines
        .into_iter()
        .map(|line| {
            let mut splits = line.splitn(2, '-');
            (
                splits.next().unwrap().to_string(),
                splits.next().unwrap().to_string(),
            )
        })
        .fold(Graph::new(), |mut graph, (start, end)| {
            graph.add_edge(&start, &end);
            graph
        });

    let paths = graph.count_paths(false);
    println!("01: {}", paths);

    let paths = graph.count_paths(true);
    println!("02: {}", paths);
}
