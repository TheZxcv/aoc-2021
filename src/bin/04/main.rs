#[derive(std::fmt::Debug)]
pub struct Board {
    pub board: Vec<Vec<i32>>,
    complete: bool,
}

impl Board {
    pub fn new(board: Vec<Vec<i32>>) -> Board {
        Board {
            board,
            complete: false,
        }
    }

    pub fn score(&self, last_called: i32) -> i32 {
        let sum_cells: i32 = self.board.iter().flatten().filter(|&&c| c != -1).sum();
        last_called * sum_cells
    }

    pub fn mark(&mut self, value: i32) -> bool {
        for row in self.board.iter_mut() {
            for cell in row.iter_mut() {
                if *cell == value {
                    *cell = -1;
                }
            }
        }

        self.complete = self.check_complete();
        self.complete
    }

    pub fn is_complete(&self) -> bool {
        self.complete
    }

    fn check_complete(&self) -> bool {
        let complete_row = self
            .board
            .iter()
            .map(|row| row.iter().all(|&c| c == -1))
            .any(|e| e);
        if complete_row {
            return true;
        }

        let columns = self.board[0].len();
        for i in 0..columns {
            let mut complete_column = true;
            for row in self.board.iter() {
                if row[i] != -1 {
                    complete_column = false;
                }
            }

            if complete_column {
                return true;
            }
        }

        false
    }
}

fn main() {
    let lines = aoc::input_lines();
    let numbers = lines[0]
        .split(',')
        .map(|num| num.parse().unwrap())
        .collect::<Vec<i32>>();

    let mut scores = Vec::new();
    let mut boards = read_boards(lines.iter().skip(1));
    for number in numbers {
        let mut all_complete = true;
        for board in boards.iter_mut() {
            if board.is_complete() {
                continue;
            }

            all_complete = false;
            if board.mark(number) {
                let score = board.score(number);
                scores.push(score);
            }
        }

        if all_complete {
            break;
        }
    }

    println!("01: {}", scores.first().unwrap());
    println!("02: {}", scores.last().unwrap());
}

fn read_boards<'a>(input: impl IntoIterator<Item = &'a String>) -> Vec<Board> {
    let mut boards: Vec<Board> = Vec::new();
    let mut board: Vec<Vec<i32>> = Vec::new();
    for line in input {
        if line == "" {
            if board.len() > 0 {
                boards.push(Board::new(board));
                board = Vec::new();
            }
            continue;
        }

        let row = line
            .split(' ')
            .filter(|&x| !x.is_empty())
            .map(|num| num.parse().unwrap())
            .collect::<Vec<i32>>();
        board.push(row);
    }

    if board.len() > 0 {
        boards.push(Board::new(board));
    }

    return boards;
}
