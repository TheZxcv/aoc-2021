pub struct Board {
    pub width: usize,
    pub height: usize,
    pub cells: Vec<Vec<i32>>,
}

impl Board {
    pub fn parse(lines: &Vec<String>) -> Board {
        let width = lines[0].len();
        let mut height = 0;
        let mut rows: Vec<Vec<i32>> = Vec::new();

        for line in lines {
            assert!(width == line.len());

            let row: Vec<i32> = line
                .chars()
                .filter_map(|c| c.to_digit(10))
                .map(|n| n as i32)
                .collect();
            rows.push(row);

            height += 1;
        }

        Board {
            width,
            height,
            cells: rows,
        }
    }

    pub fn update(&mut self) -> u32 {
        self.cells
            .iter_mut()
            .flat_map(|row| row)
            .for_each(|el| *el += 1);

        let mut flashes = 0;

        loop {
            let mut no_update = true;
            for row in 0..self.height {
                for col in 0..self.width {
                    if self.cells[row][col] > 9 {
                        no_update = false;
                        self.cells[row][col] = 0;
                        flashes += 1;

                        self.update_around(col, row);
                    }
                }
            }

            if no_update {
                break;
            }
        }

        flashes
    }

    pub fn all_flashed(&self) -> bool {
        self.cells.iter().flat_map(|row| row).all(|&el| el == 0)
    }

    fn update_around(&mut self, col: usize, row: usize) {
        // Left and Right
        if col > 0 && self.cells[row][col - 1] != 0 {
            self.cells[row][col - 1] += 1;
        }
        if col < self.width - 1 && self.cells[row][col + 1] != 0 {
            self.cells[row][col + 1] += 1;
        }

        // Up and Down
        if row > 0 && self.cells[row - 1][col] != 0 {
            self.cells[row - 1][col] += 1;
        }
        if row < self.height - 1 && self.cells[row + 1][col] != 0 {
            self.cells[row + 1][col] += 1;
        }

        // Left-Top and Left-Bottom
        if col > 0 && row > 0 && self.cells[row - 1][col - 1] != 0 {
            self.cells[row - 1][col - 1] += 1;
        }
        if col > 0 && row < self.height - 1 && self.cells[row + 1][col - 1] != 0 {
            self.cells[row + 1][col - 1] += 1;
        }

        // Right-Top and Right-Bottom
        if col < self.width - 1 && row > 0 && self.cells[row - 1][col + 1] != 0 {
            self.cells[row - 1][col + 1] += 1;
        }
        if col < self.width - 1 && row < self.height - 1 && self.cells[row + 1][col + 1] != 0 {
            self.cells[row + 1][col + 1] += 1;
        }
    }
}

fn main() {
    let lines = aoc::input_lines();
    let mut board = Board::parse(&lines);

    let mut flashes = 0;
    for _ in 0..100 {
        flashes += board.update();
    }
    println!("01: {}", flashes);

    // Assuming that they didn't all flash during the first 100 steps.
    let mut step = 100;
    while !board.all_flashed() {
        board.update();
        step += 1;
    }
    println!("02: {}", step);
}
