use std::cmp::Ordering;
use std::collections::BinaryHeap;

#[derive(Copy, Clone, Eq, PartialEq)]
struct Candidate {
    risk: i32,
    index: usize,
}

impl Ord for Candidate {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .risk
            .cmp(&self.risk)
            .then_with(|| self.index.cmp(&other.index))
    }
}

impl PartialOrd for Candidate {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub struct Node {
    risk: i32,
}

pub struct Graph {
    pub nodes: Vec<Node>,
    pub edges: Vec<Vec<usize>>,
}

impl Graph {
    pub fn parse_as_small(height: usize, width: usize, map: &Vec<Vec<i32>>) -> Graph {
        let mut nodes: Vec<Node> = Vec::new();
        let mut edges: Vec<Vec<usize>> = Vec::new();

        for i in 0..height {
            for j in 0..width {
                let index = i * height + j;
                let risk = map[i][j];
                nodes.push(Node { risk });

                assert!(edges.len() == index);
                edges.push(Vec::new());

                if j > 0 {
                    let left = index - 1;
                    edges[index].push(left);
                    edges[left].push(index);
                }

                if i > 0 {
                    let above = (i - 1) * height + j;
                    edges[index].push(above);
                    edges[above].push(index);
                }
            }
        }

        Graph { nodes, edges }
    }

    pub fn parse_as_massive(height: usize, width: usize, map: &Vec<Vec<i32>>) -> Graph {
        let mut nodes: Vec<Node> = Vec::new();
        let mut edges: Vec<Vec<usize>> = Vec::new();

        for i in 0..(5 * height) {
            for j in 0..(5 * width) {
                let wrapped_i = i % height;
                let wrapped_j = j % width;

                let risk = {
                    let repeat_i = (i / height) as i32;
                    let repeat_j = (j / width) as i32;
                    let mut risk = map[wrapped_i][wrapped_j] + repeat_i + repeat_j;

                    // Adjustment because we are skipping zero.
                    while risk >= 10 {
                        risk = risk % 10 + risk / 10;
                    }

                    if risk == 0 {
                        1
                    } else {
                        risk
                    }
                };

                let index = i * 5 * height + j;
                nodes.push(Node { risk });

                assert!(edges.len() == index);
                edges.push(Vec::new());

                if j > 0 {
                    let left = index - 1;
                    edges[index].push(left);
                    edges[left].push(index);
                }

                if i > 0 {
                    let above = (i - 1) * 5 * height + j;
                    edges[index].push(above);
                    edges[above].push(index);
                }
            }
        }

        Graph { nodes, edges }
    }

    // Reference: https://doc.rust-lang.org/std/collections/binary_heap/index.html#examples
    pub fn shortest_path_cost(&self) -> Option<i32> {
        let start = 0;
        let end = self.nodes.len() - 1;

        let mut distances = vec![i32::MAX; self.nodes.len()];
        distances[start] = 0;

        let mut queue = BinaryHeap::new();
        queue.push(Candidate {
            index: start,
            risk: 0,
        });

        while let Some(Candidate { risk, index }) = queue.pop() {
            if index == end {
                // Found the shortest path.
                return Some(risk);
            }

            // We have already a lower risk path to the `index` node.
            if risk > distances[index] {
                continue;
            }

            for &next in &self.edges[index] {
                let next = Candidate {
                    index: next,
                    risk: risk + self.nodes[next].risk,
                };

                if next.risk < distances[next.index] {
                    queue.push(next);
                    distances[next.index] = next.risk;
                }
            }
        }

        None
    }
}

fn main() {
    let lines = aoc::input_lines();
    let (height, width, map) = parse_map(&lines);

    let graph = Graph::parse_as_small(height, width, &map);
    let min_risk = graph.shortest_path_cost().unwrap();
    println!("01: {}", min_risk);

    let graph = Graph::parse_as_massive(height, width, &map);
    let min_risk = graph.shortest_path_cost().unwrap();
    println!("02: {}", min_risk);
}

fn parse_map(lines: &Vec<String>) -> (usize, usize, Vec<Vec<i32>>) {
    let width = lines[0].len();
    let mut height = 0;
    let mut map: Vec<Vec<i32>> = Vec::new();

    for line in lines {
        assert!(width == line.len());

        let row: Vec<i32> = line
            .chars()
            .filter_map(|c| c.to_digit(10))
            .map(|n| n as i32)
            .collect();
        map.push(row);

        height += 1;
    }

    (height, width, map)
}
