type Variable = usize;

type Literal = i32;

#[derive(Copy, Clone)]
enum Operand {
    Variable(Variable),
    Literal(Literal),
}

enum Instruction {
    // inp a - Read an input value and write it to variable a.
    Inp(Variable),
    // add a b - Add the value of a to the value of b, then store the result in variable a.
    Add(Variable, Operand),
    // mul a b - Multiply the value of a by the value of b, then store the result in variable a.
    Mul(Variable, Operand),
    // div a b - Divide the value of a by the value of b, truncate the result to an integer,
    // then store the result in variable a. (Here, "truncate" means to round the value toward zero.)
    Div(Variable, Operand),
    // mod a b - Divide the value of a by the value of b, then store the remainder in variable a.
    // (This is also called the modulo operation.)
    Mod(Variable, Operand),
    // eql a b - If the value of a and b are equal, then store the value 1 in variable a.
    // Otherwise, store the value 0 in variable a.
    Eql(Variable, Operand),
}

impl Instruction {
    pub fn parse(line: &str) -> Instruction {
        let mut splits = line.split_whitespace();

        let op = splits.next().unwrap();
        let destination = Instruction::read_variable(splits.next().unwrap());
        let operand = splits.next().map(Instruction::read_operand);

        match op {
            "inp" => Instruction::Inp(destination),
            "add" => Instruction::Add(destination, operand.unwrap()),
            "mul" => Instruction::Mul(destination, operand.unwrap()),
            "div" => Instruction::Div(destination, operand.unwrap()),
            "mod" => Instruction::Mod(destination, operand.unwrap()),
            "eql" => Instruction::Eql(destination, operand.unwrap()),
            _ => panic!("unexpected instruction: '{}'.", line),
        }
    }

    fn read_variable(input: &str) -> Variable {
        match input {
            "w" => 0,
            "x" => 1,
            "y" => 2,
            "z" => 3,
            _ => panic!("unexpected variable: '{}'.", input),
        }
    }

    fn read_operand(input: &str) -> Operand {
        match input {
            "w" => Operand::Variable(0),
            "x" => Operand::Variable(1),
            "y" => Operand::Variable(2),
            "z" => Operand::Variable(3),
            _ => match input.parse() {
                Ok(n) => Operand::Literal(n),
                _ => panic!("unexpected operand: '{}'.", input),
            },
        }
    }
}

impl std::fmt::Display for Operand {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let var_names = ["w", "x", "y", "z"];
        match self {
            Operand::Variable(var) => write!(f, "{}", var_names[*var]),
            Operand::Literal(value) => write!(f, "{}", value),
        }
    }
}

impl std::fmt::Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let var_names = ["w", "x", "y", "z"];
        match self {
            Instruction::Inp(var) => write!(f, "inp {}", var_names[*var]),
            Instruction::Add(var, op) => write!(f, "add {} {}", var_names[*var], op),
            Instruction::Mul(var, op) => write!(f, "mul {} {}", var_names[*var], op),
            Instruction::Div(var, op) => write!(f, "div {} {}", var_names[*var], op),
            Instruction::Mod(var, op) => write!(f, "mod {} {}", var_names[*var], op),
            Instruction::Eql(var, op) => write!(f, "eql {} {}", var_names[*var], op),
        }
    }
}

struct Alu {
    registers: [i64; 4],
    next_input: Option<i32>,
}

impl Alu {
    pub fn new() -> Alu {
        Alu {
            registers: [0, 0, 0, 0],
            next_input: None,
        }
    }

    pub fn reset(&mut self) {
        for el in &mut self.registers {
            *el = 0;
        }
        self.next_input = None;
    }

    fn eval(&mut self, instruction: &Instruction) -> bool {
        match *instruction {
            Instruction::Inp(var) => {
                if let Some(value) = self.fetch_input() {
                    self.registers[var] = value as i64;
                } else {
                    return false;
                }
            }
            Instruction::Add(var, op) => self.registers[var] += self.get_operand(op),
            Instruction::Mul(var, op) => self.registers[var] *= self.get_operand(op),
            Instruction::Div(var, op) => self.registers[var] /= self.get_operand(op),
            Instruction::Mod(var, op) => self.registers[var] %= self.get_operand(op),
            Instruction::Eql(var, op) => {
                self.registers[var] = if self.registers[var] == self.get_operand(op) {
                    1
                } else {
                    0
                };
            }
        }

        true
    }

    fn fetch_input(&mut self) -> Option<i32> {
        self.next_input.take()
    }

    pub fn execute(&mut self, input: &[i32], instructions: &[Instruction]) -> i64 {
        self.reset();

        self.next_input = input.get(0).copied();
        let mut next_input_index = 1;

        for instr in instructions {
            if !self.eval(instr) {
                break;
            }

            if self.next_input.is_none() && next_input_index < input.len() {
                self.next_input = input.get(next_input_index).copied();
                next_input_index += 1;
            }
        }

        self.registers[3]
    }

    fn get_operand(&self, operand: Operand) -> i64 {
        match operand {
            Operand::Variable(var) => self.registers[var],
            Operand::Literal(value) => value as i64,
        }
    }
}

fn number_to_input(n: i64) -> [i32; 14] {
    let mut input = [0; 14];

    let mut value = n;
    for i in (0..14).rev() {
        input[i] = (value % 10) as i32;
        value /= 10;
    }

    input
}

fn extract_parameters(range: &[Instruction]) -> (i32, i32, i32) {
    assert!(range.len() == 18);

    let b = if let Instruction::Div(_, Operand::Literal(value)) = range[4] {
        value
    } else {
        panic!();
    };

    let a = if let Instruction::Add(_, Operand::Literal(value)) = range[5] {
        value
    } else {
        panic!();
    };

    let c = if let Instruction::Add(_, Operand::Literal(value)) = range[15] {
        value
    } else {
        panic!();
    };

    (a, b, c)
}

fn _function(input: &[i32], parameters: &[(i32, i32, i32)]) -> i64 {
    let mut result = 0i64;
    for (&w, &(a, b, c)) in input.iter().zip(parameters) {
        let w = w as i64;
        let a = a as i64;
        let b = b as i64;
        let c = c as i64;

        let next = result / b;
        if ((result % 26) + a) == w {
            result = next;
        } else {
            result = 26 * next + w + c;
        }
    }

    result
}

fn possible_inputs_for_output(z: i64, (a, b, c): (i32, i32, i32)) -> Vec<(i32, i64)> {
    let a = a as i64;
    let b = b as i64;
    let c = c as i64;
    let mut inputs = vec![];
    {
        for truncated in 0..b {
            let prev_z = z * b + truncated;
            let w = (prev_z % 26) + a;
            if w >= 1 && w <= 9 {
                inputs.push((w as i32, prev_z));
            }
        }
    }

    {
        for w in (1..=9).rev() {
            let prev_z = z - c - w;
            if prev_z % 26 != 0 {
                continue;
            }
            let prev_z = prev_z / 26;

            if ((prev_z % 26) + a) != w {
                for truncated in 0..b {
                    let prev_z = prev_z * b + truncated;
                    inputs.push((w as i32, prev_z));
                }
            }
        }
    }

    inputs
}

fn main() {
    let program = aoc::input_lines()
        .iter()
        .map(|line| Instruction::parse(line))
        .collect::<Vec<_>>();

    let sections: Vec<usize> = program
        .iter()
        .enumerate()
        .filter(|(_, instr)| {
            if let Instruction::Inp(_) = instr {
                true
            } else {
                false
            }
        })
        .map(|(i, _)| i)
        .collect::<Vec<_>>();

    let mut parameters: [(i32, i32, i32); 14] = [(0, 0, 0); 14];
    for i in 0..sections.len() {
        let start = sections[i];
        let end = sections.get(i + 1).copied().unwrap_or(program.len());
        parameters[i] = extract_parameters(&program[start..end]);
    }

    let all_solutions = reverse_it(parameters.len() - 1, 0, &parameters);
    let mut alu = Alu::new();
    let all_valid = all_solutions
        .iter()
        .map(|&n| alu.execute(&number_to_input(n), &program))
        .all(|x| x == 0);
    assert!(all_valid);

    let max_solution = all_solutions.iter().max().unwrap();
    println!("01: {}", max_solution);

    let min_solution = all_solutions.iter().min().unwrap();
    println!("02: {}", min_solution);
}

fn reverse_it(i: usize, z: i64, parameters: &[(i32, i32, i32)]) -> Vec<i64> {
    let candidates = possible_inputs_for_output(z, parameters[i]);
    let mut solutions = vec![];

    for (d, z) in candidates {
        if i == 0 && z == 0 {
            solutions.push(d as i64);
        } else {
            let partial_solutions = reverse_it(i - 1, z, parameters);
            for partial in partial_solutions {
                solutions.push(partial * 10 + d as i64);
            }
        }
    }

    solutions
}
