fn main() {
    let mut grid: Vec<Vec<char>> = aoc::input_lines()
        .iter()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut steps = 0;
    loop {
        let (updated, new_grid) = update(grid);
        grid = new_grid;
        steps += 1;

        if !updated {
            break;
        }
    }

    println!("01: {}", steps);
}

fn update(mut grid: Vec<Vec<char>>) -> (bool, Vec<Vec<char>>) {
    let mut any_update = false;

    for row in &mut grid {
        let first = row[0];
        let last = *row.last().unwrap();

        let mut skip_next = false;
        for i in 0..(row.len() - 1) {
            if !skip_next && row[i] == '>' && row[i + 1] == '.' {
                row[i] = '.';
                row[i + 1] = '>';
                any_update = true;
                skip_next = true;
            } else {
                skip_next = false;
            }
        }

        if first == '.' && last == '>' {
            *row.last_mut().unwrap() = '.';
            row[0] = '>';
            any_update = true;
        }
    }

    let ncols = grid[0].len();
    for j in 0..ncols {
        let first = grid[0][j];
        let last = grid.last().unwrap()[j];

        let mut skip_next = false;
        for i in 0..(grid.len() - 1) {
            if !skip_next && grid[i][j] == 'v' && grid[i + 1][j] == '.' {
                grid[i][j] = '.';
                grid[i + 1][j] = 'v';
                any_update = true;
                skip_next = true;
            } else {
                skip_next = false;
            }
        }

        if first == '.' && last == 'v' {
            grid.last_mut().unwrap()[j] = '.';
            grid[0][j] = 'v';
            any_update = true;
        }
    }

    (any_update, grid)
}
