use std::fmt::Write;

#[derive(Copy, Clone)]
pub enum Axis {
    X,
    Y,
}

#[derive(Copy, Clone)]
pub struct FoldAlong {
    pub axis: Axis,
    pub position: usize,
}

impl FoldAlong {
    pub fn new(line: &String) -> FoldAlong {
        assert!(line.starts_with("fold along "));

        let axis = match line.chars().nth(11) {
            Some('x') => Axis::X,
            Some('y') => Axis::Y,
            _ => unreachable!(),
        };
        let position: usize = line[13..].parse().unwrap();

        FoldAlong { axis, position }
    }
}

pub struct Paper {
    width: usize,
    height: usize,
    dots: Vec<Vec<bool>>,
}

impl Paper {
    pub fn new(dots: &Vec<(usize, usize)>) -> Paper {
        let max_x = dots.iter().map(|&(x, _)| x).max().unwrap() as usize + 1;
        let max_y = dots.iter().map(|&(_, y)| y).max().unwrap() as usize + 1;
        let mut paper = Vec::new();
        for _ in 0..max_y {
            paper.push(vec![false; max_x]);
        }

        dots.iter().for_each(|&(x, y)| paper[y][x] = true);

        Paper {
            width: max_x,
            height: max_y,
            dots: paper,
        }
    }

    pub fn number_of_dots(&self) -> usize {
        self.dots
            .iter()
            .flat_map(|row| row)
            .filter(|&&el| el)
            .count()
    }

    pub fn fold(&self, fold_along: FoldAlong) -> Paper {
        match fold_along.axis {
            Axis::X => self.fold_x(fold_along.position),
            Axis::Y => self.fold_y(fold_along.position),
        }
    }

    pub fn fold_x(&self, pivot: usize) -> Paper {
        let folded_width = self.width / 2;
        assert!(pivot == folded_width);

        let mut paper = Vec::new();
        for _ in 0..self.height {
            paper.push(vec![false; folded_width]);
        }

        for y in 0..self.height {
            for x in 0..folded_width {
                paper[y][x] = self.dots[y][x] || self.dots[y][self.width - x - 1];
            }
        }

        Paper {
            width: folded_width,
            height: self.height,
            dots: paper,
        }
    }

    pub fn fold_y(&self, pivot: usize) -> Paper {
        let folded_height = self.height / 2;
        assert!(pivot == folded_height);

        let mut paper = Vec::new();
        for _ in 0..folded_height {
            paper.push(vec![false; self.width]);
        }

        for y in 0..folded_height {
            for x in 0..self.width {
                paper[y][x] = self.dots[y][x] || self.dots[self.height - y - 1][x];
            }
        }

        Paper {
            width: self.width,
            height: folded_height,
            dots: paper,
        }
    }

    pub fn to_string(&self) -> String {
        let mut output = String::new();
        for y in 0..self.height {
            for x in 0..self.width {
                if self.dots[y][x] {
                    write!(&mut output, "X").unwrap();
                } else {
                    write!(&mut output, " ").unwrap();
                }
            }
            write!(&mut output, "\n").unwrap();
        }

        output
    }
}

fn main() {
    let lines = aoc::input_lines();
    let (dots, folds) = {
        let mut splits = lines.splitn(2, |line| line.len() == 0);
        (splits.next().unwrap(), splits.next().unwrap())
    };

    let dots = dots
        .iter()
        .map(|line| {
            let mut splits = line.splitn(2, ',').map(|num| num.parse::<usize>().unwrap());
            (splits.next().unwrap(), splits.next().unwrap())
        })
        .collect::<Vec<_>>();
    let paper = Paper::new(&dots);
    let folds = folds.iter().map(FoldAlong::new).collect::<Vec<_>>();

    let paper = paper.fold(*folds.first().unwrap());
    println!("01: {}", paper.number_of_dots());

    let paper = folds
        .iter()
        .skip(1)
        .fold(paper, |paper, &fold| paper.fold(fold));
    println!("02:");
    println!("{}", paper.to_string());
}
