use std::collections::HashMap;

pub struct Entry {
    pub start: (i32, i32),
    pub end: (i32, i32),
}

impl Entry {
    pub fn new(line: &str) -> Entry {
        let splits = line.split(" -> ").collect::<Vec<_>>();
        if splits.len() != 2 {
            panic!("{}", line);
        }

        let first = splits[0]
            .split(",")
            .map(|num| num.parse().unwrap())
            .collect::<Vec<i32>>();

        let second = splits[1]
            .split(",")
            .map(|num| num.parse().unwrap())
            .collect::<Vec<i32>>();

        Entry {
            start: (first[0], first[1]),
            end: (second[0], second[1]),
        }
    }

    pub fn straight_points(&self) -> Vec<(i32, i32)> {
        if self.start.0 == self.end.0 {
            let iterator = if self.start.1 < self.end.1 {
                self.start.1..=self.end.1
            } else {
                self.end.1..=self.start.1
            };

            return iterator.map(|y| (self.start.0, y)).collect::<Vec<_>>();
        } else if self.start.1 == self.end.1 {
            let iterator = if self.start.0 < self.end.0 {
                self.start.0..=self.end.0
            } else {
                self.end.0..=self.start.0
            };

            return iterator.map(|x| (x, self.start.1)).collect::<Vec<_>>();
        } else {
            panic!("Invalid point!");
        }
    }

    pub fn points(&self) -> Vec<(i32, i32)> {
        if self.start.0 == self.end.0 {
            let iterator = if self.start.1 < self.end.1 {
                self.start.1..=self.end.1
            } else {
                self.end.1..=self.start.1
            };

            return iterator.map(|y| (self.start.0, y)).collect::<Vec<_>>();
        } else if self.start.1 == self.end.1 {
            let iterator = if self.start.0 < self.end.0 {
                self.start.0..=self.end.0
            } else {
                self.end.0..=self.start.0
            };

            return iterator.map(|x| (x, self.start.1)).collect::<Vec<_>>();
        } else {
            let x_inc = if self.start.0 < self.end.0 { 1 } else { -1 };
            let y_inc = if self.start.1 < self.end.1 { 1 } else { -1 };
            let length = (self.start.0 - self.end.0).abs();

            return (0..=length)
                .map(|i| (self.start.0 + i * x_inc, self.start.1 + i * y_inc))
                .collect::<Vec<_>>();
        }
    }
}

fn main() {
    let entries = aoc::input_lines()
        .iter()
        .map(|line| Entry::new(&line))
        .collect::<Vec<_>>();

    let overlaps = entries
        .iter()
        .filter(|entry| entry.start.0 == entry.end.0 || entry.start.1 == entry.end.1)
        .flat_map(|entry| entry.straight_points())
        .fold(HashMap::new(), |mut acc, point| {
            let counter = acc.entry(point).or_insert(0);
            *counter += 1;
            acc
        })
        .into_values()
        .filter(|&c| c > 1)
        .count();

    println!("01: {}", overlaps);

    let overlaps = entries
        .iter()
        .flat_map(|entry| entry.points())
        .fold(HashMap::new(), |mut acc, point| {
            let counter = acc.entry(point).or_insert(0);
            *counter += 1;
            acc
        })
        .into_values()
        .filter(|&c| c > 1)
        .count();

    println!("02: {}", overlaps);
}
