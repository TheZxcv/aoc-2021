fn main() {
    let lines = aoc::input_lines();
    let enhancement_pattern = lines.get(0).unwrap().chars().collect::<Vec<_>>();
    assert!(enhancement_pattern.len() == 512);

    let image: Vec<Vec<char>> = lines
        .iter()
        .skip(2)
        .take_while(|&x| !x.is_empty())
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let image = {
        let size = image.len() * 2;
        expand(image, size)
    };

    let (enhanced, background) = std::iter::repeat(0)
        .take(2)
        .fold((image, '.'), |(image, background), _| {
            enhance(image, &enhancement_pattern, background)
        });
    assert!(background == '.');

    println!("01: {}", count_lit(&enhanced));

    let (enhanced, background) = std::iter::repeat(0)
        .take(48)
        .fold((enhanced, '.'), |(image, background), _| {
            enhance(image, &enhancement_pattern, background)
        });
    assert!(background == '.');

    println!("02: {}", count_lit(&enhanced));
}

fn expand(image: Vec<Vec<char>>, size: usize) -> Vec<Vec<char>> {
    let nrows: usize = image.len();
    let ncols: usize = image.get(0).unwrap().len();
    let mut expanded: Vec<Vec<char>> = vec![vec!['.'; size]; size];

    assert!(size > nrows);
    assert!(size > ncols);

    let start_row = expanded.len() / 2 - nrows / 2;
    let start_col = expanded[0].len() / 2 - ncols / 2;
    for (i, row) in image.iter().enumerate() {
        for (j, &ch) in row.iter().enumerate() {
            expanded[start_row + i][start_col + j] = ch;
        }
    }

    expanded
}

fn enhance(image: Vec<Vec<char>>, pattern: &Vec<char>, background: char) -> (Vec<Vec<char>>, char) {
    let nrows: usize = image.len();
    let ncols: usize = image.get(0).unwrap().len();
    let mut enhanced: Vec<Vec<char>> = vec![vec!['.'; ncols]; nrows];

    for i in 0..nrows {
        for j in 0..ncols {
            let index = calculate_index(i, j, &image, background);
            enhanced[i][j] = pattern[index];
        }
    }

    if background == '.' {
        (enhanced, pattern[0])
    } else {
        (enhanced, pattern[512 - 1])
    }
}

fn calculate_index(i: usize, j: usize, image: &Vec<Vec<char>>, background: char) -> usize {
    let nrows = image.len() as isize;
    let ncols = image.get(0).unwrap().len() as isize;
    let mut index: usize = 0;

    let offsets: [(isize, isize); 9] = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 0),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    for (i_offset, j_offset) in offsets {
        index <<= 1;

        let ii = (i as isize) + i_offset;
        let jj = (j as isize) + j_offset;
        if ii >= 0 && jj >= 0 && ii < nrows && jj < ncols {
            let ii = ii as usize;
            let jj = jj as usize;

            if image[ii][jj] == '#' {
                index |= 1;
            }
        } else if background == '#' {
            index |= 1;
        }
    }

    assert!(index < 512);

    index
}

fn count_lit(image: &Vec<Vec<char>>) -> u32 {
    image
        .iter()
        .flat_map(|row| row)
        .filter(|&&ch| ch == '#')
        .count() as u32
}

#[allow(dead_code)]
fn print_image(image: &Vec<Vec<char>>) {
    image.iter().for_each(|row| {
        row.iter().for_each(|ch| print!("{}", ch));
        println!();
    });
    println!();
}
