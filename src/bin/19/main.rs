use std::collections::HashSet;

type Point = (i32, i32, i32);

fn distance(x: Point, y: Point) -> i32 {
    (x.0 - y.0).abs() + (x.1 - y.1).abs() + (x.2 - y.2).abs()
}

pub struct Scanner {
    pub position: Point,
    pub detections: Vec<Point>,
}

impl Scanner {
    pub fn parse<'a>(input: &mut impl Iterator<Item = &'a String>) -> Option<Scanner> {
        let mut detections = Vec::new();

        let header = input.next();
        if let None = header {
            return None;
        }

        assert!(header.unwrap().starts_with("--- scanner "));

        while let Some(line) = input.next() {
            if line == "" {
                break;
            }

            let row: Vec<i32> = line
                .split(',')
                .filter(|&x| !x.is_empty())
                .map(|num| num.parse().unwrap())
                .collect();
            assert!(row.len() == 3);
            detections.push((row[0], row[1], row[2]));
        }

        assert!(detections.len() > 0);
        Some(Scanner {
            position: (0, 0, 0),
            detections,
        })
    }

    pub fn match_beacons(&self, other: &Scanner) -> Option<Scanner> {
        let first_transforms: Vec<Box<Transform>> = vec![
            Box::new(|(x, y, z)| (x, y, z)),
            Box::new(|(x, y, z)| (x, z, y)),
            Box::new(|(x, y, z)| (y, x, z)),
            Box::new(|(x, y, z)| (y, z, x)),
            Box::new(|(x, y, z)| (z, x, y)),
            Box::new(|(x, y, z)| (z, y, x)),
        ];
        let second_transforms: Vec<Box<Transform>> = vec![
            Box::new(|(x, y, z)| (x, y, z)),
            Box::new(|(x, y, z)| (x, y, -z)),
            Box::new(|(x, y, z)| (x, -y, z)),
            Box::new(|(x, y, z)| (x, -y, -z)),
            Box::new(|(x, y, z)| (-x, y, z)),
            Box::new(|(x, y, z)| (-x, y, -z)),
            Box::new(|(x, y, z)| (-x, -y, z)),
            Box::new(|(x, y, z)| (-x, -y, -z)),
        ];

        for first in &first_transforms {
            for second in &second_transforms {
                let rotate = |p| second(first(p));
                for x in &self.detections {
                    for y in &other.detections {
                        let yy = rotate(*y);
                        let offset = (x.0 - yy.0, x.1 - yy.1, x.2 - yy.2);
                        let translate = |(x, y, z)| (x + offset.0, y + offset.1, z + offset.2);
                        let yy = translate(yy);
                        assert!(x.0 == yy.0 && x.1 == yy.1 && x.2 == yy.2);

                        let bb = other
                            .detections
                            .iter()
                            .map(|&p| translate(rotate(p)))
                            .collect::<Vec<_>>();

                        if Scanner::count_matching(&self.detections, &bb) >= 12 {
                            return Some(Scanner {
                                detections: bb,
                                position: offset,
                            });
                        }
                    }
                }
            }
        }

        None
    }

    fn count_matching(a: &Vec<Point>, b: &Vec<Point>) -> i32 {
        let mut count = 0;
        for x in a {
            for y in b {
                if x.0 == y.0 && x.1 == y.1 && x.2 == y.2 {
                    count += 1;
                }
            }
        }

        count
    }
}

type Transform = dyn Fn(Point) -> Point;

fn main() {
    let lines = aoc::input_lines();
    let (first, others) = {
        let mut others = Vec::new();
        let mut iter = &mut lines.iter();
        let first = Scanner::parse(&mut iter).unwrap();
        while let Some(scanner) = Scanner::parse(&mut iter) {
            others.push(scanner);
        }

        (first, others)
    };

    let mut matched_scanners = vec![first];
    let mut scanners_left = others.iter().collect::<Vec<_>>();

    while scanners_left.len() > 0 {
        let mut new_matches: Vec<Scanner> = Vec::new();

        scanners_left = {
            let mut unmatched: Vec<&Scanner> = Vec::new();
            for unknown in &scanners_left {
                let mut is_matched = false;
                for scanner in &matched_scanners {
                    if let Some(matched_scanner) = scanner.match_beacons(&unknown) {
                        new_matches.push(matched_scanner);
                        is_matched = true;
                        break;
                    }
                }

                if !is_matched {
                    unmatched.push(unknown);
                }
            }

            unmatched
        };

        for scanner in new_matches {
            matched_scanners.push(scanner);
        }
    }

    let beacons = {
        let mut uniques = HashSet::<Point>::new();
        for scanner in &matched_scanners {
            for p in &scanner.detections {
                uniques.insert(*p);
            }
        }
        uniques.len()
    };
    println!("01: {}", beacons);

    let positions = matched_scanners
        .iter()
        .map(|s| s.position)
        .collect::<Vec<_>>();
    let max_distance = positions
        .iter()
        .flat_map(|x| positions.iter().map(move |y| distance(*x, *y)))
        .max()
        .unwrap();
    println!("02: {}", max_distance);
}
