fn main() {
    let values: Vec<i32> = aoc::input_lines()
        .iter()
        .map(|line| line.parse().unwrap())
        .collect();

    let mut previous = None;
    let mut increased = 0;
    for current in &values {
        if let Some(prev) = previous {
            if prev < current {
                increased += 1;
            }
        }
        previous = Some(current);
    }

    println!("01: {}", increased);

    let mut previous = None;
    let mut count = 0;
    for i in 0..values.len() - 2 {
        let current = values[i] + values[i + 1] + values[i + 2];

        if let Some(prev) = previous {
            if prev < current {
                count += 1;
            }
        }
        previous = Some(current);
    }

    println!("02: {}", count);
}
