fn main() {
    let lines = aoc::input_lines();

    let bits = lines[0].len();
    let mut counters = vec![0; bits];
    for line in &lines {
        assert!(counters.len() == line.len(), "line of different length!");

        for i in 0..counters.len() {
            if line.chars().nth(i) == Some('1') {
                counters[i] += 1;
            }
        }
    }

    let mut gamma_rate = 0;
    let mut epsilon_rate = 0;
    for i in 0..bits {
        let mask = 1 << (bits - i - 1);
        if counters[i] >= lines.len() - counters[i] {
            gamma_rate |= mask;
        } else {
            epsilon_rate |= mask;
        }
    }

    let oxygen_generator_rating = {
        let mut candidates = lines.clone();
        for i in 0..bits {
            let ones = candidates
                .iter()
                .filter(|line| line.chars().nth(i) == Some('1'))
                .count();
            let zeros = candidates.len() - ones;

            let most_common = if ones >= zeros { '1' } else { '0' };
            candidates = candidates
                .iter()
                .cloned()
                .filter(|line| line.chars().nth(i) == Some(most_common))
                .collect::<Vec<_>>();

            if candidates.len() == 1 {
                break;
            }
        }

        candidates[0].clone()
    };

    let co2_scrubber_rating = {
        let mut candidates = lines.clone();
        for i in 0..bits {
            let ones = candidates
                .iter()
                .filter(|line| line.chars().nth(i) == Some('1'))
                .count();
            let zeros = candidates.len() - ones;

            let least_common = if ones >= zeros { '0' } else { '1' };
            candidates = candidates
                .iter()
                .cloned()
                .filter(|line| line.chars().nth(i) == Some(least_common))
                .collect::<Vec<_>>();

            if candidates.len() == 1 {
                break;
            }
        }

        candidates[0].clone()
    };

    println!("01: {}", gamma_rate * epsilon_rate);
    println!(
        "02: {}",
        to_int(&oxygen_generator_rating) * to_int(&co2_scrubber_rating)
    );
}

fn to_int(bits: &str) -> i32 {
    let mut value = 0;
    for bit in bits.chars() {
        value <<= 1;
        if bit == '1' {
            value |= 1;
        }
    }

    return value;
}
