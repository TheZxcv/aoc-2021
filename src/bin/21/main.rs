use std::collections::HashMap;

pub struct Dice {
    next_value: i32,
    pub rolls: i32,
}

impl Dice {
    pub fn new(seed: i32) -> Dice {
        Dice {
            next_value: seed,
            rolls: 0,
        }
    }

    pub fn next(&mut self) -> i32 {
        self.rolls += 1;

        let value = self.next_value;
        self.next_value += 1;
        if self.next_value == 101 {
            self.next_value = 1;
        }

        value
    }
}

pub struct Player {
    pub score: i32,
    pub position: i32,
}

impl Player {
    pub fn new(staring_position: i32) -> Player {
        Player {
            score: 0,
            position: staring_position,
        }
    }

    pub fn handle_turn(&mut self, dice: &mut Dice) {
        self.position = (self.position + dice.next() + dice.next() + dice.next()) % 10;
        if self.position == 0 {
            self.position = 10;
        }

        self.score += self.position;
    }

    pub fn has_won(&self) -> bool {
        self.score >= 1000
    }
}

fn main() {
    let (player_one_initial, player_two_initial) = {
        let input = aoc::input_lines();
        assert!(input[0].starts_with("Player 1 starting position: "));
        assert!(input[1].starts_with("Player 2 starting position: "));

        let player_one = input[0][28..].parse::<i32>().unwrap();
        let player_two = input[1][28..].parse::<i32>().unwrap();
        (player_one, player_two)
    };

    let mut player_one = Player::new(player_one_initial);
    let mut player_two = Player::new(player_two_initial);
    let mut dice = Dice::new(1);
    while !player_one.has_won() && !player_two.has_won() {
        player_one.handle_turn(&mut dice);
        if player_one.has_won() {
            break;
        }

        player_two.handle_turn(&mut dice);
        if player_two.has_won() {
            break;
        }
    }

    let product = if player_one.has_won() {
        player_two.score * dice.rolls
    } else {
        player_one.score * dice.rolls
    };

    println!("01: {}", product);

    let (one_victories, two_victories) = count_victories(player_one_initial, player_two_initial);
    println!("02: {}", i64::max(one_victories, two_victories));
}

pub fn count_victories(player_one: i32, player_two: i32) -> (i64, i64) {
    let mut cache = HashMap::<(i32, i32, i32, i32), (i64, i64)>::new();
    handle_turn_cached(player_one, 0, player_two, 0, &mut cache)
}

pub fn handle_turn_cached(
    position_one: i32,
    score_one: i32,
    position_two: i32,
    score_two: i32,
    mut cache: &mut HashMap<(i32, i32, i32, i32), (i64, i64)>,
) -> (i64, i64) {
    let cached = cache.get(&(position_one, score_one, position_two, score_two));
    if let Some(value) = cached {
        return *value;
    }

    let possible_outcomes: [(i32, i32); 7] = [
        // (sum of dices, universes count)
        (3, 1),
        (4, 3),
        (5, 6),
        (6, 7),
        (7, 6),
        (8, 3),
        (9, 1),
    ];

    let mut victories_one = 0;
    let mut victories_two = 0;
    for (roll_one, universes_one) in possible_outcomes {
        let position_one_next = next_position(position_one, roll_one);
        let score_one_next = score_one + position_one_next;
        if score_one_next >= 21 {
            victories_one += universes_one as i64;
            continue;
        }

        let (one, two) = {
            let mut victories_one = 0;
            let mut victories_two = 0;
            for (roll_two, universes_two) in possible_outcomes {
                let position_two_next = next_position(position_two, roll_two);
                let score_two_next = score_two + position_two_next;
                if score_two_next >= 21 {
                    victories_two += universes_two as i64;
                    continue;
                }

                let (one, two) = handle_turn_cached(
                    position_one_next,
                    score_one_next,
                    position_two_next,
                    score_two_next,
                    &mut cache,
                );

                victories_one += (universes_two as i64) * one;
                victories_two += (universes_two as i64) * two;
            }

            (victories_one, victories_two)
        };

        victories_one += (universes_one as i64) * one;
        victories_two += (universes_one as i64) * two;
    }

    cache.insert(
        (position_one, score_one, position_two, score_two),
        (victories_one, victories_two),
    );

    (victories_one, victories_two)
}

fn next_position(position: i32, roll: i32) -> i32 {
    let score = (position + roll) % 10;
    if score == 0 {
        10
    } else {
        score
    }
}
