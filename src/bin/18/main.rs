pub enum Node {
    Leaf(i32),
    Node(Box<Node>, Box<Node>),
}

pub enum State {
    None,
    Exploded,
    ExplodingBoth(i32, i32),
    ExplodingLeft(i32),
    ExplodingRight(i32),
}

impl Node {
    pub fn parse(line: &mut impl Iterator<Item = char>) -> Node {
        match line.next() {
            Some('[') => {
                let left = Node::parse(line);
                assert!(line.next() == Some(','));

                let right = Node::parse(line);
                assert!(line.next() == Some(']'));

                Node::Node(Box::new(left), Box::new(right))
            }
            Some(digit) if digit.is_digit(10) => {
                let value = digit.to_digit(10).unwrap() as i32;
                Node::Leaf(value)
            }
            _ => panic!(),
        }
    }

    pub fn add(self, other: Node) -> Node {
        let mut sum = Node::Node(Box::new(self), Box::new(other));

        loop {
            sum = sum.explode();
            let (any_split, result) = sum.split_leftmost();

            sum = result;
            if !any_split {
                return sum;
            }
        }
    }

    fn explode(self) -> Node {
        let mut result = self;
        let mut any_explode = true;
        while any_explode {
            let (any, new_res) = result.explode_leftmost();
            any_explode = any;
            result = new_res;
        }

        return result;
    }

    pub fn split_leftmost(self) -> (bool, Node) {
        match self {
            Node::Leaf(value) => {
                if value >= 10 {
                    let left = Node::Leaf(value / 2);
                    let right = Node::Leaf(value / 2 + value % 2);
                    (true, Node::Node(Box::new(left), Box::new(right)))
                } else {
                    (false, Node::Leaf(value))
                }
            }
            Node::Node(left, right) => {
                let (is_split, left) = left.split_leftmost();
                if is_split {
                    (true, Node::Node(Box::new(left), right))
                } else {
                    let (is_split, right) = right.split_leftmost();
                    (is_split, Node::Node(Box::new(left), Box::new(right)))
                }
            }
        }
    }

    pub fn explode_leftmost(self) -> (bool, Node) {
        let (state, tree) = self.explode_leftmost_rec(0);
        if let State::None = state {
            (false, tree)
        } else {
            (true, tree)
        }
    }

    fn explode_leftmost_rec(self, depth: i32) -> (State, Node) {
        match self {
            Node::Leaf(_) => (State::None, self),
            Node::Node(left, right) => {
                let (left, right) = (*left, *right);
                if depth >= 4 {
                    if let (Node::Leaf(left), Node::Leaf(right)) = (&left, &right) {
                        return (State::ExplodingBoth(*left, *right), Node::Leaf(0));
                    }
                }

                let (state, left) = left.explode_leftmost_rec(depth + 1);
                if let State::ExplodingBoth(x, y) = state {
                    return (
                        State::ExplodingLeft(x),
                        Node::Node(Box::new(left), Box::new(right.add_to_leftmost(y))),
                    );
                } else if let State::ExplodingLeft(x) = state {
                    return (
                        State::ExplodingLeft(x),
                        Node::Node(Box::new(left), Box::new(right)),
                    );
                } else if let State::ExplodingRight(y) = state {
                    return (
                        State::Exploded,
                        Node::Node(Box::new(left), Box::new(right.add_to_leftmost(y))),
                    );
                } else if let State::Exploded = state {
                    return (state, Node::Node(Box::new(left), Box::new(right)));
                }

                let (state, right) = right.explode_leftmost_rec(depth + 1);
                if let State::ExplodingBoth(x, y) = state {
                    return (
                        State::ExplodingRight(y),
                        Node::Node(Box::new(left.add_to_rightmost(x)), Box::new(right)),
                    );
                } else if let State::ExplodingLeft(x) = state {
                    return (
                        State::Exploded,
                        Node::Node(Box::new(left.add_to_rightmost(x)), Box::new(right)),
                    );
                } else if let State::ExplodingRight(y) = state {
                    return (
                        State::ExplodingRight(y),
                        Node::Node(Box::new(left), Box::new(right)),
                    );
                }

                return (state, Node::Node(Box::new(left), Box::new(right)));
            }
        }
    }

    fn add_to_leftmost(self, increment: i32) -> Node {
        let (_, tree) = self.add_to_leftmost_rec(increment);
        tree
    }

    fn add_to_leftmost_rec(self, increment: i32) -> (bool, Node) {
        match self {
            Node::Leaf(value) => (true, Node::Leaf(value + increment)),
            Node::Node(left, right) => {
                let (added, left) = left.add_to_leftmost_rec(increment);
                if added {
                    (true, Node::Node(Box::new(left), right))
                } else {
                    let (added, right) = right.add_to_leftmost_rec(increment);
                    (added, Node::Node(Box::new(left), Box::new(right)))
                }
            }
        }
    }

    fn add_to_rightmost(self, increment: i32) -> Node {
        let (_, tree) = self.add_to_rightmost_rec(increment);
        tree
    }

    fn add_to_rightmost_rec(self, increment: i32) -> (bool, Node) {
        match self {
            Node::Leaf(value) => (true, Node::Leaf(value + increment)),
            Node::Node(left, right) => {
                let (added, right) = right.add_to_rightmost_rec(increment);
                if added {
                    (true, Node::Node(left, Box::new(right)))
                } else {
                    let (added, left) = left.add_to_rightmost_rec(increment);
                    (added, Node::Node(Box::new(left), Box::new(right)))
                }
            }
        }
    }

    fn magnitude(&self) -> u64 {
        match self {
            Node::Leaf(value) => *value as u64,
            Node::Node(left, right) => 3 * left.magnitude() + 2 * right.magnitude(),
        }
    }

    #[allow(dead_code)]
    fn to_string(&self) -> String {
        match self {
            Node::Leaf(value) => {
                format!("{}", value)
            }
            Node::Node(left, right) => {
                format!("[{},{}]", left.to_string(), right.to_string())
            }
        }
    }
}

fn main() {
    let lines = aoc::input_lines();

    let mut tree = Node::parse(&mut lines[0].chars());
    for line in lines.iter().skip(1) {
        let other = Node::parse(&mut line.chars());
        tree = tree.add(other);
    }
    println!("01: {}", tree.magnitude());

    let max_magnitude = lines
        .iter()
        .flat_map(|x| lines.iter().map(move |y| (x, y)))
        .filter(|(x, y)| x != y)
        .map(|(x, y)| {
            let x = Node::parse(&mut x.chars());
            let y = Node::parse(&mut y.chars());
            x.add(y).magnitude()
        })
        .max()
        .unwrap();
    println!("02: {}", max_magnitude);
}

#[cfg(test)]
mod tests {
    use crate::Node;

    #[test]
    fn test_explode() {
        let samples = vec![
            ("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"),
            ("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"),
            ("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]"),
            (
                "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]",
                "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
            ),
            (
                "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
                "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
            ),
        ];

        for (input, expected) in samples {
            let tree = Node::parse(&mut input.chars());
            let (_, tree) = tree.explode_leftmost();
            assert_eq!(tree.to_string(), expected);
        }
    }

    #[test]
    fn test_simple1() {
        let a = Node::parse(&mut "[1,2]".chars());
        let b = Node::parse(&mut "[[3,4],5]".chars());
        assert_eq!(a.add(b).to_string(), "[[1,2],[[3,4],5]]");
    }

    #[test]
    fn test_simple2() {
        let a = Node::parse(&mut "[[[[4,3],4],4],[7,[[8,4],9]]]".chars());
        let b = Node::parse(&mut "[1,1]".chars());
        assert_eq!(a.add(b).to_string(), "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
    }

    #[test]
    fn test_many1() {
        let terms = vec!["[1,1]", "[2,2]", "[3,3]", "[4,4]"];
        let mut tree = Node::parse(&mut terms[0].chars());
        for line in terms.into_iter().skip(1) {
            let other = Node::parse(&mut line.chars());
            tree = tree.add(other);
        }

        assert_eq!(tree.to_string(), "[[[[1,1],[2,2]],[3,3]],[4,4]]");
    }

    #[test]
    fn test_many2() {
        let terms = vec!["[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]"];
        let mut tree = Node::parse(&mut terms[0].chars());
        for line in terms.into_iter().skip(1) {
            let other = Node::parse(&mut line.chars());
            tree = tree.add(other);
        }

        assert_eq!(tree.to_string(), "[[[[3,0],[5,3]],[4,4]],[5,5]]");
    }

    #[test]
    fn test_many3() {
        let terms = vec!["[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]"];
        let mut tree = Node::parse(&mut terms[0].chars());
        for line in terms.into_iter().skip(1) {
            let other = Node::parse(&mut line.chars());
            tree = tree.add(other);
        }

        assert_eq!(tree.to_string(), "[[[[5,0],[7,4]],[5,5]],[6,6]]");
    }

    #[test]
    fn test_many4() {
        let terms = vec![
            ("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]", ""),
            (
                "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
                "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]",
            ),
            (
                "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]",
                "[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]",
            ),
            (
                "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]",
                "[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]",
            ),
            (
                "[7,[5,[[3,8],[1,4]]]]",
                "[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]",
            ),
            (
                "[[2,[2,2]],[8,[8,1]]]",
                "[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]",
            ),
            ("[2,9]", "[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]"),
            (
                "[1,[[[9,3],9],[[9,0],[0,7]]]]",
                "[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]",
            ),
            (
                "[[[5,[7,4]],7],1]",
                "[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]",
            ),
            (
                "[[[[4,2],2],6],[8,7]]",
                "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]",
            ),
        ];
        let mut tree = Node::parse(&mut terms[0].0.chars());
        for (line, expected) in terms.into_iter().skip(1) {
            let other = Node::parse(&mut line.chars());
            tree = tree.add(other);

            assert_eq!(tree.to_string(), expected);
        }
    }

    #[test]
    fn test_many5() {
        let terms = vec![
            "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
            "[[[5,[2,8]],4],[5,[[9,9],0]]]",
            "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
            "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
            "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
            "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
            "[[[[5,4],[7,7]],8],[[8,3],8]]",
            "[[9,3],[[9,9],[6,[4,9]]]]",
            "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
            "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
        ];
        let mut tree = Node::parse(&mut terms[0].chars());
        for line in terms.into_iter().skip(1) {
            let other = Node::parse(&mut line.chars());
            tree = tree.add(other);
        }

        assert_eq!(
            tree.to_string(),
            "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
        );
    }
}
