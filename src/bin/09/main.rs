pub struct Matrix {
    pub width: usize,
    pub height: usize,
    pub items: Vec<Vec<i32>>,
}

impl Matrix {
    pub fn parse(lines: &Vec<String>) -> Matrix {
        let width = lines[0].len();
        let mut height = 0;
        let mut rows: Vec<Vec<i32>> = Vec::new();

        for line in lines {
            assert!(width == line.len());

            let row: Vec<i32> = line
                .chars()
                .filter_map(|c| c.to_digit(10))
                .map(|n| n as i32)
                .collect();
            rows.push(row);

            height += 1;
        }

        Matrix {
            width,
            height,
            items: rows,
        }
    }

    pub fn low_points(&self) -> Vec<((usize, usize), i32)> {
        let mut points = Vec::new();
        for row in 0..self.height {
            for col in 0..self.width {
                if self.is_low_point(col, row) {
                    let value = self.items[row][col];
                    points.push(((col, row), value));
                }
            }
        }

        points
    }

    fn is_low_point(&self, col: usize, row: usize) -> bool {
        let value = self.items[row][col];
        if col > 0 && self.items[row][col - 1] <= value {
            return false;
        }
        if col < self.width - 1 && self.items[row][col + 1] <= value {
            return false;
        }

        if row > 0 && self.items[row - 1][col] <= value {
            return false;
        }
        if row < self.height - 1 && self.items[row + 1][col] <= value {
            return false;
        }

        return true;
    }

    pub fn basin_size(&self, col: usize, row: usize) -> usize {
        let mut markers = Vec::<Vec<bool>>::new();
        for _ in 0..self.height {
            markers.push(vec![false; self.width]);
        }

        self.basin_size_inner(col, row, &mut markers)
    }

    fn basin_size_inner(&self, col: usize, row: usize, markers: &mut Vec<Vec<bool>>) -> usize {
        if markers[row][col] {
            return 0;
        }
        markers[row][col] = true;

        let mut size: usize = 1;
        if col > 0 && self.items[row][col - 1] != 9 {
            size += self.basin_size_inner(col - 1, row, markers)
        }
        if col < self.width - 1 && self.items[row][col + 1] != 9 {
            size += self.basin_size_inner(col + 1, row, markers)
        }

        if row > 0 && self.items[row - 1][col] != 9 {
            size += self.basin_size_inner(col, row - 1, markers)
        }
        if row < self.height - 1 && self.items[row + 1][col] != 9 {
            size += self.basin_size_inner(col, row + 1, markers)
        }

        size
    }
}

pub fn top_three(v: &Vec<usize>) -> Vec<usize> {
    use std::collections::BinaryHeap;
    let mut heap = v.iter().copied().collect::<BinaryHeap<usize>>();
    let mut top_three = Vec::new();
    for _ in 0..3 {
        if let Some(v) = heap.pop() {
            top_three.push(v);
        }
    }

    top_three
}

fn main() {
    let lines = aoc::input_lines();
    let matrix = Matrix::parse(&lines);

    let points = matrix.low_points();
    let sum_risks: i32 = points.iter().map(|(_, value)| value + 1).sum();

    println!("01: {}", sum_risks);

    let basins = points
        .iter()
        .map(|((col, row), _)| matrix.basin_size(*col, *row))
        .collect::<Vec<_>>();
    let product: usize = top_three(&basins).iter().fold(1, |a, b| a * b);

    println!("02: {}", product);
}
