use std::collections::HashMap;

fn main() {
    let first_line = aoc::input_lines().first().unwrap().clone();
    let values: Vec<i32> = first_line
        .split(',')
        .map(|line| line.parse().unwrap())
        .collect();

    let mut cache = HashMap::<(i32, i32), usize>::new();

    let after_80_days: usize = values
        .iter()
        .map(|&age| calculate_population_memoized(age, 80, &mut cache))
        .sum();
    println!("01: {}", after_80_days);

    let after_256_days: usize = values
        .iter()
        .map(|&age| calculate_population_memoized(age, 256, &mut cache))
        .sum();
    println!("02: {}", after_256_days);
}

fn calculate_population_memoized(
    age: i32,
    days: i32,
    mut cache: &mut HashMap<(i32, i32), usize>,
) -> usize {
    let cached = cache.get(&(age, days));
    if let Some(value) = cached {
        return *value;
    }

    if days == 0 {
        cache.insert((age, days), 1);
        return 1;
    } else if age == 0 {
        let population = calculate_population_memoized(6, days - 1, &mut cache)
            + calculate_population_memoized(8, days - 1, &mut cache);
        cache.insert((age, days), population);
        return population;
    } else {
        let population = calculate_population_memoized(age - 1, days - 1, &mut cache);
        cache.insert((age, days), population);
        return population;
    }
}
